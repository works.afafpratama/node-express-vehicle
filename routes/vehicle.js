const express = require('express');
const router = express.Router();
const vehicles = require('../services/vehicle');

router.get('/', async function(req, res, next) {
  try {
    res.json(await vehicles.getMultiple(req.query.page));
  } catch (err) {
    console.error(`Error while getting vehicles `, err.message);
    next(err);
  }
});

router.post('/', async function(req, res, next) {
  try {
    res.json(await vehicles.create(req.body));
  } catch (err) {
    console.error(`Error while creating vehicle`, err.message);
    next(err);
  }
});

router.put('/:id', async function(req, res, next) {
  try {
    res.json(await vehicles.update(req.params.id, req.body));
  } catch (err) {
    console.error(`Error while updating vehicle`, err.message);
    next(err);
  }
});

router.delete('/:id', async function(req, res, next) {
  try {
    res.json(await vehicles.remove(req.params.id));
  } catch (err) {
    console.error(`Error while deleting vehicle`, err.message);
    next(err);
  }
});

module.exports = router;