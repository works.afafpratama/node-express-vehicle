var express = require('express');
const { status } = require('express/lib/response');
var app = express();
app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const mysql = require('mysql')
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'express',
  port: 3309
})

connection.connect();

app.get('/', function (req, res) {
    connection.query('SELECT * FROM vehicle', (err, rows, fields) => {
        if (err) throw err

        res.json('The solution is: ' + rows[0].vehicle);
    })

   //  connection.query('SELECT 1 + 1 AS solution', (err, rows, fields) => {
   //      if (err) throw err

   //      res.send('The solution is: ' + rows[0].solution);
   //  })
})

// This responds a POST request for the homepage
app.post('/', function (req, res) {
   console.log("Got a POST request for the homepage");
   res.send('Hello POST');
})

// This responds a DELETE request for the /del_user page.
app.delete('/del_user', function (req, res) {
   console.log("Got a DELETE request for /del_user");
   res.send('Hello DELETE');
})

// This responds a GET request for the /list_user page.
app.get('/list_user', function (req, res) {
   console.log("Got a GET request for /list_user");
   res.send('Page Listing');
})

// This responds a GET request for abcd, abxcd, ab123cd, and so on
app.get('/ab*cd', function(req, res) {   
   console.log("Got a GET request for /ab*cd");
   res.send('Page Pattern Match');
})

var server = app.listen(8087, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})

// connection.end()