const db = require('./db');
const helper = require('../helper');
const config = require('../config');

async function getMultiple(page = 1){
  const offset = helper.getOffset(page, config.listPerPage);
  const rows = await db.query(
    `SELECT * FROM vehicle LIMIT ${offset},${config.listPerPage}`
  );
  const data = helper.emptyOrRows(rows);
  const meta = {page};

  return {
    data,
    meta
  }
}

async function create(vehicle){
  const result = await db.query(
    `INSERT INTO vehicle 
    (vehicle_id, vehicle_type, lock_status, speed, battery, status, location) 
    VALUES 
    (${vehicle.vehicle_id}, "${vehicle.vehicle_type}", "${vehicle.lock_status}", ${vehicle.speed}, ${vehicle.battery}, "${vehicle.status}", "${vehicle.location}")`
  );

  let message = 'Error in creating vehicle';

  if (result.affectedRows) {
    message = 'Vehicle created successfully';
  }

  return {message};
}

async function update(id, vehicle){
  const result = await db.query(
    `UPDATE vehicle 
    SET vehicle_id=${vehicle.vehicle_id}, vehicle_type="${vehicle.vehicle_type}", lock_status="${vehicle.lock_status}", speed=${vehicle.speed},
    battery=${vehicle.battery}, status="${vehicle.status}", location="${vehicle.location}"
    WHERE id=${id}` 
  );

  let message = 'Error in updating vehicle';

  if (result.affectedRows) {
    message = 'Vehicle updated successfully';
  }

  return {message};
}

async function remove(id){
  const result = await db.query(
    `DELETE FROM vehicle WHERE id=${id}`
  );

  let message = 'Error in deleting vehicle';

  if (result.affectedRows) {
    message = 'Vehicle deleted successfully';
  }

  return {message};
}

module.exports = {
  getMultiple,
  create,
  update,
  remove
}